var express = require('express');
var  methods= require('./Methods.js');

var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use(express.static('xeon'));

app.get('/index.html', function (req, res) {
    res.sendFile( __dirname + "/xeon/" + "index.html" );
});

app.get('/getFilm', function (req, res) {

    try {

        var id = req.param('id');
        var lunghezzaLati = req.param('altro');
        res.json(searchFilm(id));

    }catch(err) {
        res.json({"error": true, "message":"Errore nei parametri d'input"});
    }

});

app.get('/getSearchFilm', function (req, res) {

    try {

        var id = req.param('id');
        var x = req.param('x');
        var y = req.param('y');
        var z = req.param('z');
        var w = req.param('w');

        //res.json(searchFilm(Math.floor(Math.random() * (4777 - 0 + 1) ) + 0));
res.json(lastF(x,y,z,w));
    }catch(err) {
        res.json({"error": true, "message":"Errore nei parametri d'input"});
    }

});

function lastF(x,y,z,w){
    var data=getDataSetJSON();
    //console.log(data);
    for(var f=0;f<data.length;f++){
        //console.log(data[f]);

        var dist=Math.sqrt(
            (Math.pow(x-parseFloat(data[f].x.replace(",", ".")),2))+
            (Math.pow(y-parseFloat(data[f].y.replace(",", ".")),2))+
            (Math.pow(z-parseFloat(data[f].z.replace(",", ".")),2))+
            (Math.pow(w-parseFloat(data[f].w.replace(",", ".")),2))
        );


        data[f].d=dist;

    }
    var min=100;
    var minObj=new Object;
    for(var c=0;c<data.length;c++){
        if(data[c].d<min){
            min=data[c].d;
            minObj=data[c];
        }


    }

    return minObj;


}

console.log(lastF(0,0,0,0));


function searchFilm(id){

    var data=getDataSetJSON();
    for(var c=0;c<data.length;c++){
        //console.log(data[c].id);
        if(data[c].id==id){

            return data[c];
        }

    }

}




function getDataSetJSON(){

    var fs = require('fs');



    var data=fs.readFileSync('xeon/dataSet/dataset4.csv','utf8');
//console.log(data);
    var jsonDataFilm=[];
    var lines=data.split(/\r?\n/);
    for(var i=0;i<lines.length;i++){
//lines.length
        //console.log(lines[i]+"            -"+i);
        var lineSplitted=lines[i].split(";");

        var idFilm=lineSplitted[0];
        var title=lineSplitted[1];
        var imageUrl=lineSplitted[2];
        var x=lineSplitted[3];
        var y=lineSplitted[4];
        var z=lineSplitted[5];
        var w=lineSplitted[6];
        var q=lineSplitted[7];

        jsonDataFilm.push({"id":idFilm,
            "title":title,
            "imageUrl":imageUrl,
            "x":x,
            "y":y,
            "z":z,
            "w":w,
            "q":q
        });

    }

    //console.log(jsonDataFilm);
    return jsonDataFilm;




}

function getTheClosestItem(){
    var Json=getDataSetJSON();
    //console.log(Json);


}


getTheClosestItem();



app.listen(4156, function () {
    console.log('Server in esecuzione sulla porta 4156...');
});